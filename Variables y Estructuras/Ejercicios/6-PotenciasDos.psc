Proceso potenciasDeDos
	
	Definir potMax Como Entero;
	Definir  i Como Entero;
	Escribir "Ingresa el cantidad de potencias";
	Leer PotMax;
	
	Para i<-0 Hasta PotMax Con Paso 1 Hacer
		Escribir 2^i;
	FinPara
FinProceso
