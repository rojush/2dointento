Proceso ejercicio5	
	Definir numRandom,i, contadores,aux, datoARemplazar,datoRemplazo como entero;
	Dimension  numRandom[100], contadores[21];
	
	//Inicializo contadores en cero
	Para i<-0 hasta 20 Hacer
		contadores[i] <- 0;
	FinPara
	
	//Cargo los numeros aleatorios
	Para i<-0 hasta 99 Hacer
		numRandom[i] <- Aleatorio(0,20);
	FinPara
	
	//Remplazo un dato por otro
	Escribir "Ingrese el dato que quiere remplazar";
	Leer datoARemplazar;
	Escribir "Ingrese el dato por el cual lo remplazara";
	Leer datoRemplazo;
	
	Para i<-0 hasta 99 Hacer
		Si numRandom[i] = datoARemplazar Entonces
			numRandom[i] <- datoRemplazo;
		FinSi
	FinPara
	
	
	//Repeticion de numeros
	Para i<-0 hasta 99 Hacer
		aux <-numRandom[i];
		contadores[aux] <-  contadores[aux] + 1;
	FinPara
	
	//Mostrar la repeticion de cada numero
	Para i<-0 hasta 20 Hacer
		Escribir i," se repite ", contadores[i], " veces";		
	FinPara
	
FinProceso
