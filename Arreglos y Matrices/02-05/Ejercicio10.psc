Proceso ejercicio10
	Definir i,j, matriz, diagonalPrincipal Como Entero;
	Dimension matriz[5,5], diagonalPrincipal[5];
	
	Para i<-0 hasta 4 Hacer
		Para j<-0 hasta 4 Hacer
			matriz[i,j]<-Aleatorio(-5,15);
			Escribir Sin Saltar "[ ",matriz[i,j]," ]";
			
			Si i=j Entonces
				diagonalPrincipal[i]<-matriz[i,j];
			FinSi
		FinPara
		Escribir "";
	FinPara
	Escribir "";
	Escribir sin saltar "Diagonal Principal: [";
	Para i<-0 hasta 4 Hacer
		Escribir sin saltar diagonalPrincipal[i],",";
	FinPara
	Escribir "]";
FinProceso
