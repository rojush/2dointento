Proceso ejercicio2
	definir i, numAleatorios Como Entero;
	Dimension numAleatorios[20];
	
	Para i<-0 hasta 19 Hacer
		numAleatorios[i]<-Aleatorio(-10,10);
	FinPara
	
	Escribir sin saltar "Numeros aleatorios: [";
	Para i<-0 hasta 19 Hacer
		Escribir sin saltar numAleatorios[i],",";
	FinPara
	Escribir sin saltar "]";
FinProceso
