Proceso Ejercicio3
	definir i, numAleatorios, cantPositivos,cantNegativos,cantCeros Como Entero;
	
	Dimension numAleatorios[20];
	cantPositivos<-0;
	cantCeros<-0;
	cantNegativos<-0;
	
	Para i<-0 hasta 19 Hacer
		numAleatorios[i]<-Aleatorio(-10,10);
		Si numAleatorios[i] > 0 Entonces
			cantPositivos<-cantPositivos+1;
		Sino 
			Si numAleatorios[i] < 0 Entonces
				cantNegativos<-cantNegativos+1;
			SiNo
				cantCeros<-cantCeros+1;
			FinSi			
		FinSi
	FinPara	
	
	Escribir sin saltar "Numeros aleatorios: [";
	Para i<-0 hasta 19 Hacer
		Escribir sin saltar numAleatorios[i],",";
	FinPara	
	Escribir "]";
	
	Escribir "Cantidad de positivos: ", cantPositivos;
	Escribir "Cantidad de negativos: ", cantNegativos;
	Escribir "Cantidad de ceros: ", cantCeros;
	
	
	
FinProceso
